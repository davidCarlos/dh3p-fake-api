// hello.js
module.exports = (req, res, next) => {
  if (req.method == 'POST') {
    var request = require('request');
    request.post(
      'http://0.0.0.0:3000/plugin/denouncements/update',
      {json:
        { person: { name: 'ze', email: 'ze@mail.com' },
        dncmt: {
          city: 'Foo',
          description: 'some violation',
          token: "o6sppV5I6GnYFbwWvOQgQA==",
          protocol: "12345678"
        },
        victims: [{ name: 'maria', contact: 'maria@mail.com' }],
        offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
      }},
      function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log(body)
        }
      }
    );
  }
  next()
}
